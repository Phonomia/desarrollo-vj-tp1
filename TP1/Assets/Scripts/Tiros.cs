﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tiros : MonoBehaviour {
	
	public GameObject[] array_bola;
	int inicial = 0;
	public Text tiros;
	// Use this for initialization
	void Awake()
	{
		for (int i = 0; i < array_bola.Length; i++) 
		{
			if (i == inicial)
				array_bola [i].SetActive (true);
			else
			array_bola [i].SetActive (false);
		}
		tiros.text = "Tiros: " + array_bola.Length;
	}
	public void Next_Bola()
	{
		inicial++;
		tiros.text = "Tiros: " + (array_bola.Length -inicial);
		if (inicial < array_bola.Length) 
		{
			array_bola [inicial].SetActive (true);
		}
	}
}
