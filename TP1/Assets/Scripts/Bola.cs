﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bola : MonoBehaviour 
{
	
	public float Speed_Move = 5;
	public bool State_initial = true;
	public bool State_force = false;
	public float force = 30;

	public float timer = 0.0f;

	public Text fuerza;
	public GameObject tiros;
	private int force_int =0;
	private Rigidbody m_rigibody;
	// Use this for initialization


	void Awake()
	{
		m_rigibody = GetComponent<Rigidbody> ();
	}
	void Start () 
	{
	}
	// Update is called once per frame
	void Update () 
	{
		if (State_initial) 
		{
			float Translation = Input.GetAxis ("Horizontal") * Speed_Move * Time.deltaTime;
			transform.Translate (-1*Translation, 0, 0);
			if (Input.GetKeyDown(KeyCode.Space)) 
			{
				State_initial = false;
				State_force = true;
			}
		} 
		else 
		{
			if (State_force == true) {
				fuerza.text = "Fuerza" + force_int;
				if (force_int == 100)
					force_int = 0;
				force_int++;
				if (Input.GetKeyDown (KeyCode.Space)) {
					m_rigibody.AddForce (0, 0, -1 * force_int * force);
					State_force = false;
				}
			} 
			else 
			{
				if (this.m_rigibody.velocity.z == 0) 
				{
					tiros.SendMessage ("Next_Bola");
					gameObject.SetActive (false);
				}
			}
		}
	}
}
